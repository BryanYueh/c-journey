#include <iostream>
#include <sstream>
#include <string>

using namespace std;

bool verifyArgv(int argc, char *argv[]);
void showResult(double v1, double v2, double (*fncPtr)(double, double));
double addResult(double v1, double v2);
double subResult(double v1, double v2);
double mutipleResult(double v1, double v2);
double divideResult(double v1, double v2);

int main(int argc, char *argv[])
{
    cout << "Quiz 7 Callback" << endl;

    for(int i = 0; i < argc; i++){
        cout << argv[i] << endl;
    }

    //verify argv
    if(!verifyArgv(argc, argv)){
        cout << "arguments incorrect!" << endl;
        return 0;
    }

    double dVaule1, dValue2;
    char *cOp;
    stringstream ss;
    ss << argv[1];
    ss >> dVaule1;
    ss.str("");
    ss.clear();
    cOp = argv[2];
    ss << argv[3];
    ss >> dValue2;
    ss.str("");
    ss.clear();

    if(*cOp == '+')
        showResult(dVaule1, dValue2, addResult);
    if(*cOp == '-')
        showResult(dVaule1, dValue2, subResult);
    if(*cOp == 'x')
        showResult(dVaule1, dValue2, mutipleResult);
    if(*cOp == '/')
        showResult(dVaule1, dValue2, divideResult);

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();
    return 0;
}

bool verifyArgv(int argc, char *argv[]){
    if(argc < 4)
        return false;

    stringstream ss;
    double dValue1;
    ss << argv[1];
    ss >> dValue1;
    ss.str("");
    ss.clear();

    char *cOp;
    cOp = argv[2];

    ss << argv[3];
    double dValue2;
    ss >> dValue2;
    ss.str("");
    ss.clear();

    return true;
}

void showResult(double v1, double v2, double (*fncPtr)(double, double)){
    cout << fncPtr(v1, v2) << endl;
}

double addResult(double v1, double v2){
    cout << v1 << " + " << v2 << " = ";
    return (v1 + v2);
}
double subResult(double v1, double v2){
    cout << v1 << " - " << v2 << " = ";
    return (v1 - v2);
}
double mutipleResult(double v1, double v2){
    cout << v1 << " * " << v2 << " = ";
    return (v1 * v2);
}
double divideResult(double v1, double v2){
    cout << v1 << " / " << v2 << " = ";
    return (v1 / v2);
}
