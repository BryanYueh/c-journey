#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

bool verify(int ans, int input);
int pickAValue();
int getRandomNumber(int min, int max);
bool isPlayAgain();

int main()
{
    cout << "Quiz 5.x.2" << endl;

    while(true){
        int nAns(pickAValue()); //setup answer
        cout << "Ans=" << nAns << endl;
        cout << "Let's play a game.  I'm thinking of a number.  You have 7 tries to guess what it is." << endl;
        int nInput;
        bool bWon(false);
        for(int i = 1; i <= 7; i++){
            cout << "Guess #" << i << ": ";
            cin.clear();
            cin >> nInput;
            bWon = verify(nAns, nInput);
            if(bWon){
                cout << "You win!" << endl;
                break;
            }
        }
        if(!bWon){
            cout << "You sucks!" << endl;
        }

        if(isPlayAgain())
            ;
        else{
            cout << "Thank you for playing." << endl;
            return 0;
        }
    }

    return 0;
}
bool isPlayAgain(){
    cout << "Would you like to play again (y/n)? ";
    char cInput;

    do{
        cin.clear();
        cin.ignore(32767, '\n');
        cin >> cInput;
    }while(cInput != 'Y' && cInput != 'y' && cInput != 'N' && cInput != 'n');

    if(cInput == 'Y' || cInput == 'y')
        return true;
    else
        return false;
}

bool verify(int ans, int input){
    if(ans == input)
        return true;
    else{
        if(ans > input)
            cout << "Your guess is too low." << endl;
        else
            cout << "Your guess is too high." << endl;

        return false;
    }
}

int pickAValue(){
    srand(static_cast<unsigned int>(time(0)));
    rand();
    return getRandomNumber(1, 100);
}

int getRandomNumber(int min, int max){
    static const double fraction = 1.0 / (static_cast<double>(RAND_MAX) + 1.0);  // static used for efficiency, so we only calculate this value once
    // evenly distribute the random number across our range
    return static_cast<int>(rand() * fraction * (max - min + 1) + min);
}
