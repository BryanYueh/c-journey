#include <iostream>

using namespace std;

int main()
{
    cout << "Quiz 6.3.2" << endl;

    const int arrayLength(9);
    int array[arrayLength] = { 4, 6, 7, 3, 8, 2, 1, 9, 5 };

    cout << "the array = ";
    for(int i = 0; i < arrayLength; i++){
        cout << array[i];
        if(i < arrayLength-1)
            cout << ",";
        else
            cout << endl;
    }

    int nInput;
    do{
        cout << "Display which one(1 - 9): ";
        cin.clear();
        cin >> nInput;
    }while(!(nInput <= 9 && nInput >= 1));

    cout << "The #" << nInput << " element: " << array[nInput - 1] << endl;

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}
