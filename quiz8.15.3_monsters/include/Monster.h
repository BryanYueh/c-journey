#ifndef MONSTER_H
#define MONSTER_H
#include <iostream>
#include <string>

class Monster
{
    public:
        enum MonsterType{
            Dragon,
            Goblin,
            Ogre,
            Orc,
            Skeleton,
            Troll,
            Vampire,
            Zombie,
            MAX_MONSTER_TYPES
        };

        Monster(MonsterType mt, std::string name, std::string roar, int hp);
        std::string getTypeString();
        print();

    private:
        MonsterType m_type;
        std::string m_name;
        std::string m_roar;
        int m_hp;

};

#endif // MONSTER_H
