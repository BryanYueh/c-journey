#ifndef MONSTERGENERATOR_H
#define MONSTERGENERATOR_H
#include "Monster.h"
#include <random>
#include <string>
#include <time.h>

class MonsterGenerator
{
    public:
        MonsterGenerator();
        static Monster generateMonster();
        static int getRandomNumber(int min, int max);
        static std::string s_name[];
        static std::string s_roar[];

    private:

};

#endif // MONSTERGENERATOR_H
