/************************************************************************
*  Project:
*  Function:
*  Describe:
*************************************************************************
*  : FBND
*  : Bryan Yueh
*************************************************************************
*  Copyright 2016 by Burst Entertainment Inc.
************************************************************************/

#include "MonsterGenerator.h"

MonsterGenerator::MonsterGenerator()
{

}

Monster MonsterGenerator::generateMonster(){
    Monster::MonsterType nType = static_cast<Monster::MonsterType>(getRandomNumber(0, Monster::MAX_MONSTER_TYPES - 1));
    std::string strName = s_name[getRandomNumber(0, 5)];
    std::string strRoar = s_roar[getRandomNumber(0, 5)];
    int nHp = getRandomNumber(1, 100);

    return Monster(nType, strName, strRoar, nHp);
}

int MonsterGenerator::getRandomNumber(int min, int max)
{
    double fraction = 1.0 / (static_cast<double>(RAND_MAX) + 1.0);
    return static_cast<int>(rand() * fraction * (max - min + 1) + min);
}

std::string MonsterGenerator::s_name[] = {"NameA", "NameB", "NameC", "NameD", "NameE", "NameF"};
std::string MonsterGenerator::s_roar[] = {"Roar11", "Roar22", "Roar33", "Roar44", "Roar55", "Roar66"};
