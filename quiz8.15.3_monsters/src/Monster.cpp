/************************************************************************
*  Project:
*  Function:
*  Describe:
*************************************************************************
*  : FBND
*  : Bryan Yueh
*************************************************************************
*  Copyright 2016 by Burst Entertainment Inc.
************************************************************************/

#include "Monster.h"

Monster::Monster(MonsterType mt, std::string name, std::string roar, int hp)
{
    m_type = mt;
    m_name = name;
    m_roar = roar;
    m_hp = hp;
}

Monster::print(){
    std::cout << m_name << " the ";
    std::string strName = getTypeString();
    std::cout << strName << " has ";
    std::cout << m_hp << " hit points and says ";
    std::cout << m_roar << std::endl;
}

std::string Monster::getTypeString(){
    switch(m_type){
    case Dragon:
        return "Dragon";
    case Goblin:
        return "Goblin";
    case Ogre:
        return "Ogre";
    case Orc:
        return "Orc";
    case Skeleton:
        return "Skeleton";
    case Troll:
        return "Troll";
    case Vampire:
        return "Vampire";
    case Zombie:
        return "Zombie";
    }

    return "???";
}
