#include <iostream>
#include "Monster.h"
#include "MonsterGenerator.h"

using namespace std;

int main()
{
    cout << "Quiz 8.15.3 Monsters" << endl;

    //Monster skele(Monster::Skeleton, "Bones", "*rattle*", 4);
    //skele.print();

    srand(static_cast<unsigned int>(time(0)));
    rand();

    for(int i = 0; i < 10; i++){
        Monster m = MonsterGenerator::generateMonster();
        m.print();
    }

    cin.clear();
	cin.ignore(32767, '\n');
	cin.get();

    return 0;
}
