#ifndef BJVIEW_H_INCLUDED
#define BJVIEW_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include "bjModel.h"

char inputLoop(std::string str);
double inputLoop(std::string str, double dMin, double dMax);
int inputLoop(std::string str, const std::vector<std::string> &strs);
void debugInfo(bool isDebug, std::string str, const std::vector<int> &v);
void displayStrings(const std::string &str);
void displayStrings(const std::vector<std::string> &str);
void displayWelcome();
std::string convertNumber2String(const int n);
std::string convertNumber2String(const double n);
void updateTableInfo(statFlags flags, std::vector<int> nzCardsBank,
                     std::vector<int> nzCardsPlayer, bool isShowAll,
                     PointPosibility &ppBank, PointPosibility &ppPlayer);
void showCards(Gamer gamer, const std::vector<int> &v, bool isShowAll,
               PointPosibility &ppBank, PointPosibility &ppPlayer);
std::string showRealCards(const int number);

#endif // BJVIEW_H_INCLUDED
