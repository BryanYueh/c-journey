#ifndef BJMODEL_H_INCLUDED
#define BJMODEL_H_INCLUDED

enum CardType{
    SPADE,
    HEART,
    DIMAND,
    CLUB
};

enum CardPoint{
    NUMBER_A,
    NUMBER_2,
    NUMBER_3,
    NUMBER_4,
    NUMBER_5,
    NUMBER_6,
    NUMBER_7,
    NUMBER_8,
    NUMBER_9,
    NUMBER_10,
    FACE_J,
    FACE_Q,
    FACE_K
};

enum Gamer{
    BANKER,
    PLAYER
};

struct PointPosibility{
    int Max;
    int Min;
};

struct statFlags{
    int Round;
    double Gold;
    double Bet;
    double Insurances;
    double DoubleBet;
    bool isBet;
    bool isInsurances;
    bool isDouble;
    bool isSurrender;
    bool isBankerBlackJack;
    bool isPlayerBlackJack;
};

void initialPointPosibility();
void initialCardQue();

#endif // BJMODEL_H_INCLUDED
