/************************************************************************
*  Project:  Black Jack Console
*  Function: Control
*  Describe: Controller
*************************************************************************
*  : FBND
*  : Bryan Yueh
*************************************************************************
*  Copyright 2016 by Burst Entertainment Inc.
************************************************************************/

#include "bjControl.h"

using namespace std;

vector<int> nzCardQue;
vector<int> nzCardsBank;
vector<int> nzCardsPlayer;
statFlags flags;
PointPosibility bankerPointPosibility;
PointPosibility playerPointPosibility;
int nCurCardInQue = 0;
bool isEndRound = false;
const bool isDebug = false;

void start(){
    //initial
    initialization();

    //display welcome
    displayWelcome();

    char sInputStart;
    //main loop
    while(true){
        clearFlags();
        //resize banker/player cards array
        nzCardsBank.resize(0);
        nzCardsPlayer.resize(0);

        isEndRound = false;
        nCurCardInQue = 0;

        sInputStart = inputLoop("Shall we play?(y/n): ");
        if(sInputStart == 'n' || sInputStart == 'N'){
            break;
        }else if(flags.Gold < 5.0){
            displayStrings("You'd lost every cent! Got out of my casino!");
            break;
        }

        //dealer send cards
        sendCards();

        //if player got black jack?
        checkPlayerBJ();

        //if banker got an A, ask player to buy insurances or not
        ask2BuyInsurance();

        //ask player to Hit/Stand/double/surrender?
        int nInputAction = askDoubleSurrender();

        switch(nInputAction){
        case 1: //hit
            displayStrings("You call a card.");
            break;
        case 2: //stand
            displayStrings("You stop calling cards.");
            break;

        case 3: //double
            flags.DoubleBet = flags.Bet;
            flags.Gold -= flags.DoubleBet;
            flags.isDouble = true;

            displayStrings("You double bet!");

            //player got one more card
            nzCardsPlayer.resize(nzCardsPlayer.size()+1);
            dealer(PLAYER);

            //update pointPosibility
            bankerPointPosibility = getTotalPoints(BANKER);
            playerPointPosibility = getTotalPoints(PLAYER);

            //update table info
            updateTableInfo(flags, nzCardsBank, nzCardsPlayer, false, bankerPointPosibility, playerPointPosibility);
            break;

        case 4: //surrender
            flags.isSurrender = true;
            break;
        }

        //if surrender
        if(flags.isSurrender){
            flags.Gold += flags.Bet/2.0;
            string strTemp = "You surrender, and can get " + convertNumber2String(flags.Bet/2.0) + " back.";
            displayStrings(strTemp);
        }else{
            while(true){
                //hit
                if(nInputAction == 1){
                    while(true){
                        nzCardsPlayer.resize(nzCardsPlayer.size()+1);
                        dealer(PLAYER);

                        //update pointPosibility
                        bankerPointPosibility = getTotalPoints(BANKER);
                        playerPointPosibility = getTotalPoints(PLAYER);

                        //update table info
                        updateTableInfo(flags, nzCardsBank, nzCardsPlayer, false, bankerPointPosibility, playerPointPosibility);

                        if(isSave(PLAYER)){
                            if(getMaxPoints(PLAYER) == 21){
                                nInputAction = 2;
                                break;
                            }

                            nInputAction = askHitStand();

                            if(nInputAction == 2){    //stand
                                displayStrings("You stop calling cards.");
                                break;
                            }

                        }else{
                            //bust
                            nInputAction = 2;
                            break;
                        }
                    }
                }else{
                    break;
                }
            }

            //is banker black jack?
            if(isBlackJack(nzCardsBank.at(0), nzCardsBank.at(1))){
                flags.isBankerBlackJack = true;
                displayStrings("Banker got Black Jack!");

                //update pointPosibility
                bankerPointPosibility = getTotalPoints(BANKER);
                playerPointPosibility = getTotalPoints(PLAYER);

                //update table info
                updateTableInfo(flags, nzCardsBank, nzCardsPlayer, true, bankerPointPosibility, playerPointPosibility);
            }

            //insurances result
            if(flags.isInsurances){    //bought insurances
                //if banker is black jack, player wins
                if(flags.isBankerBlackJack){
                    flags.Gold += (flags.Insurances * 2.0);
                    string strTemp = "Banker is Black Jack, and you got " + convertNumber2String(flags.Insurances * 2.0) + " back.";
                }else{
                    displayStrings("Banker is not Black Jack, and you lose your insurances bet.");
                }

                //clear isInsurances, dInsurances
                flags.Insurances = 0;
                flags.isInsurances = false;

                //update pointPosibility
                bankerPointPosibility = getTotalPoints(BANKER);
                playerPointPosibility = getTotalPoints(PLAYER);

                //update table info
                updateTableInfo(flags, nzCardsBank, nzCardsPlayer, true, bankerPointPosibility, playerPointPosibility);
            }

            //if banker is black jack
            if(flags.isBankerBlackJack){
                if(flags.isPlayerBlackJack){
                    //draw
                    string strTemp = "Draw game! and you can take your bet " + convertNumber2String(flags.Bet) + " back.";
                    displayStrings(strTemp);
                    flags.Gold += flags.Bet;
                    isEndRound = true;
                }else{
                    displayStrings("You lose. Good luck next time!");
                    isEndRound = true;
                }
            }

            //check if player bust
            if(!isSave(PLAYER)){
                displayStrings("Player bust!");
                displayStrings("You lose. Good luck next time!");
                isEndRound = true;
            }

            if(!isEndRound){
                //deal to banker if less than 17 points
                while(isLessThan17() && isSave(BANKER)){
                    displayStrings("Banker got one more card.");

                    nzCardsBank.resize(nzCardsBank.size()+1);
                    dealer(BANKER);

                    //update pointPosibility
                    bankerPointPosibility = getTotalPoints(BANKER);
                    playerPointPosibility = getTotalPoints(PLAYER);

                    //update table info
                    updateTableInfo(flags, nzCardsBank, nzCardsPlayer, true, bankerPointPosibility, playerPointPosibility);
                }

                //compare
                if(isSave(BANKER)){
                    displayStrings("Comparing!");

                    //update table info
                    updateTableInfo(flags, nzCardsBank, nzCardsPlayer, true, bankerPointPosibility, playerPointPosibility);

                    //display banker and player points
                    displayStrings("Banker: " + convertNumber2String(getMaxPoints(BANKER)));
                    displayStrings("Player: " + convertNumber2String(getMaxPoints(PLAYER)));

                    if(flags.isPlayerBlackJack){
                        double dTemp = flags.Bet + flags.Bet * 1.5;
                        string strTemp = "You win " + convertNumber2String(dTemp) + " gold!";
                        displayStrings(strTemp);
                        flags.Gold += dTemp;
                    }else{
                        if(getMaxPoints(BANKER) > getMaxPoints(PLAYER)){
                            displayStrings("You lose. Good luck next time!");
                        }else if(getMaxPoints(BANKER) == getMaxPoints(PLAYER)){
                            //draw
                            string strTemp = "Draw game! and you can take your bet " + convertNumber2String(flags.Bet) + " back.";
                            displayStrings(strTemp);
                            flags.Gold += flags.Bet + flags.DoubleBet;
                        }else{
                            double dTemp = (flags.Bet + flags.DoubleBet) * 2.0;
                            string strTemp = "You win " + convertNumber2String(dTemp) + " gold!";
                            displayStrings(strTemp);
                            flags.Gold += dTemp;
                        }
                    }
                    isEndRound = true;
                }else{
                    //banker bust, and player wins
                    displayStrings("Banker bust!");
                    if(flags.isPlayerBlackJack){
                        double dTemp = flags.Bet + flags.Bet * 1.5;
                        string strTemp = "You win " + convertNumber2String(dTemp) + " gold!";
                        displayStrings(strTemp);
                        flags.Gold += dTemp;
                    }else{
                        double dTemp = (flags.Bet + flags.DoubleBet) * 2.0;
                        string strTemp = "You win " + convertNumber2String(dTemp) + " gold!";
                        displayStrings(strTemp);
                        flags.Gold += dTemp;
                    }
                    isEndRound = true;
                }
            }
        }
    }

    displayStrings("Thanks for playing.");

    return;
}

int getRandomRange(int min, int max){
    srand(static_cast<unsigned int>(time(0)));
    rand();
    static const double fraction = 1.0 / (static_cast<double>(RAND_MAX) + 1.0);  // static used for efficiency, so we only calculate this value once
    // evenly distribute the random number across our range
    return static_cast<int>(rand() * fraction * (max - min + 1) + min);
}

vector<int> bridge(vector<int> &cQue){
    vector<int> deck;
    deck.assign(52, 0);
    for(int i=0, j=0; i<deck.size(); i+=2, j++){
        deck[i] = cQue[j];
        deck[i+1] = cQue[j+26];
    }

    if(isDebug){
        displayStrings("after bridge in function");
        for(auto &i:cQue){
            cout << i << " ";
        }
        cout << endl;
    }

    return deck;
}

vector<int> reorder(vector<int> &cQue){
    vector<int> temp;
    temp.assign(52, 0);
    int nTier = getRandomRange(3, 6);
    for(int i=0; i<nTier; i++){
        int nCut = getRandomRange(18, 34);
        for(int i=0, j=nCut; j<cQue.size(); i++, j++){
            temp[i] = cQue[j];
        }
        for(int k=(temp.size() - nCut), m=0;k<temp.size(); k++, m++){
            temp[k] = cQue[m];
        }
    }

    if(isDebug){
        cout << "after reorder in function" << endl;
        for(auto &i:temp){
            cout << i << " ";
        }
        cout << endl;
    }

    return temp;
}

void getRandomDeck(std::vector<int> &cQue){
    int nMassTimes = getRandomRange(20, 30);
    //cout << "massTimes: " << nMassTimes << endl;

    for(int j=0; j<nMassTimes; j++){
        int nAction = getRandomRange(1,100);
        //cout << "nAction: " << nAction << endl;

        if(nAction < 51){
            cQue = bridge(cQue);
        }else{
            cQue = reorder(cQue);
        }
    }

    debugInfo(isDebug, "after getRandomDeck in function", cQue);
}

void dealer(Gamer gamer){
    switch(gamer){
    case BANKER:
        nzCardsBank.at(nzCardsBank.size()-1) = nzCardQue.at(nCurCardInQue);
        if(isDebug){
            cout << "nzCardsBank[" << nzCardsBank.size()-1 << "] = " << nzCardQue.at(nCurCardInQue) << endl;
            cout << endl;
        }
        break;
    case PLAYER:
        nzCardsPlayer.at(nzCardsPlayer.size()-1) = nzCardQue.at(nCurCardInQue);
        if(isDebug){
            cout << "nzCardsPlayer[" << nzCardsPlayer.size()-1 << "] = " << nzCardQue.at(nCurCardInQue) << endl;
            cout << endl;
        }
        break;
    }

    nCurCardInQue++;
}

bool isPointTen(int &n){
    if(n % 13 == 9 ||
       n % 13 == 10 ||
       n % 13 == 11 ||
       n % 13 == 12){
        return true;
       }else{
        return false;
       }
}

bool isPointA(int &n){
    if(n % 13 == 0)
        return true;
    else
        return false;
}

int getCardPoints(int &n){
    return (n % 13) + 1;
}

bool isPosibleBlackJack(int &n1){
    if(isPointA(n1))
        return true;

    return false;
}

bool isBlackJack(int &n1, int &n2){
    if(isPointA(n1) && isPointTen(n2))
        return true;
    if(isPointA(n2) && isPointTen(n1))
        return true;

    return false;
}

PointPosibility getTotalPoints(Gamer gamer){
    vector<int> *ptr;
    int nTotalPoints = 0;
    int nACount = 0;
    PointPosibility pp;

    switch(gamer){
    case BANKER:
        ptr = &nzCardsBank;
        break;
    case PLAYER:
        ptr = &nzCardsPlayer;
        break;
    }

    //calculate # of A, and nTotalPoints without A
    for(auto &i:*ptr){
        if(isPointA(i))
            nACount++;
        else if(isPointTen(i))
            nTotalPoints += 10;
        else
            nTotalPoints += getCardPoints(i);
    }

    //calculate A
    if(nACount > 0){
        if(nACount == 1){
            if(nTotalPoints + 11 <= 21){
                pp.Max = nTotalPoints + 11;
                pp.Min = nTotalPoints + 1;
            }else{
                pp.Max = nTotalPoints + 1;
                pp.Min = nTotalPoints + 1;
            }

        }else{
            if(nACount -1 + 11 + nTotalPoints <= 21){
                pp.Max = nTotalPoints += nACount -1 + 11;
                pp.Min = nTotalPoints += nACount;
            }else{
                pp.Max = nTotalPoints += nACount;
                pp.Min = nTotalPoints += nACount;
            }
        }

    }else{
        pp.Max = nTotalPoints;
        pp.Min = nTotalPoints;
    }

    return pp;
}

bool isSave(Gamer gamer){
    switch(gamer){
    case BANKER:
        bankerPointPosibility = getTotalPoints(BANKER);
        if(bankerPointPosibility.Max <= 21 || bankerPointPosibility.Min <= 21)
            return true;

        break;
    case PLAYER:
        playerPointPosibility = getTotalPoints(PLAYER);
        if(playerPointPosibility.Max <= 21 || playerPointPosibility.Min <= 21)
            return true;

        break;
    }

    return false;
}

bool isLessThan17(){
    bankerPointPosibility = getTotalPoints(BANKER);
    if(bankerPointPosibility.Max == 21 || bankerPointPosibility.Min == 21)
        return false;

    if(bankerPointPosibility.Max >= 17 || bankerPointPosibility.Min >= 17)
        return false;

    return true;
}

int getMaxPoints(Gamer gamer){
    PointPosibility *ptr;

    switch(gamer){
    case BANKER:
        ptr = &bankerPointPosibility;
        break;
    case PLAYER:
        ptr = &playerPointPosibility;
        break;
    }

    if(ptr->Max <= 21)
        return ptr->Max;
    else
        return ptr->Min;
}

void initialization(){
    initialPointPosibility();
    initialCardQue();
    flags.Round = 0;
    flags.Gold = 10000.0;
    flags.Bet = 0;
    flags.Insurances = 0;
    flags.DoubleBet = 0;
    flags.isBet = false;
    flags.isInsurances = false;
    flags.isDouble = false;
    flags.isSurrender = false;
    flags.isBankerBlackJack = false;
    flags.isPlayerBlackJack = false;
}

void clearFlags(){
    flags.Bet = 0;
    flags.Insurances = 0;
    flags.DoubleBet = 0;
    flags.isBet = false;
    flags.isInsurances = false;
    flags.isDouble = false;
    flags.isSurrender = false;
    flags.isBankerBlackJack = false;
    flags.isPlayerBlackJack = false;
}

void sendCards(){
    //update nRound
    flags.Round++;
    //update table info
    updateTableInfo(flags, nzCardsBank, nzCardsPlayer, false, bankerPointPosibility, playerPointPosibility);

    //ask player to bet
    string strTemp = "How much do you want to bet(5-" + convertNumber2String(flags.Gold) + "): ";
    flags.Bet = inputLoop(strTemp, 5, flags.Gold);

    //update isBet, dGold
    flags.isBet = true;
    flags.Gold -= flags.Bet;

    //update table info
    updateTableInfo(flags, nzCardsBank, nzCardsPlayer, false, bankerPointPosibility, playerPointPosibility);

    //set up a new deck
    getRandomDeck(nzCardQue);
    debugInfo(isDebug, "After massing up, the deck is:", nzCardQue);

    //resize banker/player cards array
    nzCardsBank.resize(1);
    nzCardsPlayer.resize(1);

    //player got one card
    dealer(PLAYER);

    //banker got one one card
    dealer(BANKER);

    //resize banker/player cards array
    nzCardsBank.resize(2);
    nzCardsPlayer.resize(2);

    //player got another card face down(x)
    dealer(PLAYER);

    //dealer got another card
    dealer(BANKER);

    //update pointPosibility
    bankerPointPosibility = getTotalPoints(BANKER);
    playerPointPosibility = getTotalPoints(PLAYER);

    //update table info
    updateTableInfo(flags, nzCardsBank, nzCardsPlayer, false, bankerPointPosibility, playerPointPosibility);
}

void checkPlayerBJ(){
    if(isBlackJack(nzCardsPlayer.at(0), nzCardsPlayer.at(1))){
        flags.isPlayerBlackJack = true;
        displayStrings("Player got Black Jack!");
        //update table info
        updateTableInfo(flags, nzCardsBank, nzCardsPlayer, false, bankerPointPosibility, playerPointPosibility);
    }
}

void ask2BuyInsurance(){
    char sInputInsurance;
    if(isPosibleBlackJack(nzCardsBank.at(0))){
        while(true){
            sInputInsurance = inputLoop("Insurances(y/n)? ");
            if(sInputInsurance == 'n' || sInputInsurance == 'N')
                break;

            if((sInputInsurance == 'y' || sInputInsurance == 'Y') &&
                flags.Gold < flags.Bet/2.0){
                displayStrings("You don't have enough gold to buy an insurance!");
                break;
            }else{
                flags.Insurances = flags.Bet/2.0;
                flags.isInsurances = true;
                flags.Gold -= flags.Insurances;

                //update table info
                updateTableInfo(flags, nzCardsBank, nzCardsPlayer, false, bankerPointPosibility, playerPointPosibility);
                break;
            }
        }
    }
}

int askDoubleSurrender(){
    int nInputAction;
    vector<string> strArray;
    strArray.push_back("Hit.");
    strArray.push_back("Stand.");
    strArray.push_back("Double.");
    strArray.push_back("Surrender.");

    while(true){
        nInputAction = inputLoop("What are you doing now", strArray);
        if(nInputAction == 3 && flags.Gold < flags.Bet){
            displayStrings("You don't have enough gold to double bet!");
        }else{
            break;
        }
    }
    return nInputAction;
}

int askHitStand(){
    vector<string> strArray;
    strArray.push_back("Hit.");
    strArray.push_back("Stand.");

    return inputLoop("What are you doing now", strArray);
}

void initialPointPosibility(){
    bankerPointPosibility.Max = 0;
    bankerPointPosibility.Min = 0;
    playerPointPosibility.Max = 0;
    playerPointPosibility.Min = 0;
}

void initialCardQue(){
    nzCardQue.resize(52);

    //reset a new deck
    for(auto i = 0; i < 52; i++){
        nzCardQue.at(i) = i;
    }
}



