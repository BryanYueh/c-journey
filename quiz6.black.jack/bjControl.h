#ifndef BJCONTROL_H_INCLUDED
#define BJCONTROL_H_INCLUDED

#include <iostream>
#include <random>
#include <ctime>
#include <vector>
#include <string>
#include "bjModel.h"
#include "bjView.h"

void start();
int getRandomRange(int min, int max);
void getRandomDeck(std::vector<int> &cQue);
std::vector<int> bridge(std::vector<int> &cQue);
std::vector<int> reorder(std::vector<int> &cQue);
void dealer(Gamer gamer);
bool isPointTen(int &n);
bool isPointA(int &n);
int getCardPoints(int &n);
bool isPosibleBlackJack(int &n1);
bool isBlackJack(int &n1, int &n2);
PointPosibility getTotalPoints(Gamer gamer);
bool isSave(Gamer gamer);
bool isLessThan17();
int getMaxPoints(Gamer gamer);
void initialization();
void clearFlags();
void sendCards();
void checkPlayerBJ();
void ask2BuyInsurance();
int askDoubleSurrender();
int askHitStand();
void initialPointPosibility();
void initialCardQue();

#endif // BJCONTROL_H_INCLUDED
