/************************************************************************
*  Project:  Black Jack Console
*  Function: View
*  Describe: Interactive with user
*************************************************************************
*  : FBND
*  : Bryan Yueh
*************************************************************************
*  Copyright 2016 by Burst Entertainment Inc.
************************************************************************/

#include "bjView.h"

using namespace std;

char inputLoop(string str){
    char sInputStart;
    while(true){
        cout << str;
        cin.clear();
        cin.ignore(32767, '\n');
        cin >> sInputStart;

        if(sInputStart == 'y' || sInputStart == 'Y' || sInputStart == 'n' || sInputStart == 'N')
            break;
    }
    cout << endl;

    return sInputStart;
}

double inputLoop(string str, double dMin, double dMax){
    double dReturn;
    do{
        cout << str;
        cin.clear();
        cin.ignore(32767, '\n');
        cin >> dReturn;
    }while(dReturn < dMin || dReturn > dMax);
    cout << endl;

    return dReturn;
}

int inputLoop(string str, const vector<string> &strs){
    string strTemp = str + "(";
    for(int i = 0; i < strs.size(); i++){
        strTemp += convertNumber2String(i+1) + "-" + strs.at(i);
    }
    strTemp += ")? ";

    int nInputAction;
    while(true){
        cout << strTemp;
        cin.clear();
        cin.ignore(32767, '\n');
        cin >> nInputAction;

        if(nInputAction >= 1 && nInputAction <= strs.size())
            break;
    }
    cout << endl;
}

void debugInfo(bool isDebug, string str, const vector<int> &v){
    if(isDebug){
        cout << str << endl;
        for(auto &i:v){
            cout << i << " ";
        }
        cout << endl;
    }
}

void displayStrings(const string &str){
    cout << str << endl << endl;
}

void displayStrings(const vector<string> &str){
    for(auto &i:str){
        cout << i << endl;
    }
    cout << endl;
}

void displayWelcome(){
    vector<string> strz;
    strz.push_back("******************************************************************");
    strz.push_back("Welcome to Burst Casino");
    strz.push_back("Rules as below:");
    strz.push_back("1.Bet from 5-10,000.");
    strz.push_back("2.Insurances: 1/2 bet points to win 1 bet points.");
    strz.push_back("3.Black Jack: Any A + 10/any face card can win 1.5 X bet points.");
    strz.push_back("4.Surrender: Take 1/2 bet back.");
    strz.push_back("5.Double: 2 cards allowed.");
    strz.push_back("******************************************************************");
    displayStrings(strz);
}

string convertNumber2String(const int n){
    stringstream ss;
    ss << n;
    return ss.str();
}

string convertNumber2String(const double n){
    stringstream ss;
    ss << n;
    return ss.str();
}

void updateTableInfo(statFlags flags, std::vector<int> nzCardsBank,
                     std::vector<int> nzCardsPlayer, bool isShowAll,
                     PointPosibility &ppBank, PointPosibility &ppPlayer){
    string strTemp, strRound, strGold, strBet, strInsurances, strDouble;
    stringstream ss;
    ss << flags.Round;
    strRound = ss.str();
    ss.str("");
    ss.clear();
    ss << flags.Gold;
    strGold = ss.str();
    ss.str("");
    ss.clear();
    strTemp = "Round " + strRound + " Gold: " + strGold + " ";

    if(flags.Bet > 0){
        ss << flags.Bet;
        strBet = ss.str();
        ss.str("");
        ss.clear();
        strTemp += "Bet: " + strBet + " ";
    }

    if(flags.Insurances > 0){
        ss << flags.Insurances;
        strInsurances = ss.str();
        ss.str("");
        ss.clear();
        strTemp += "Insurances: " + strInsurances + " ";
    }

    if(flags.DoubleBet > 0){
        ss << flags.DoubleBet;
        strDouble = ss.str();
        ss.str("");
        ss.clear();
        strTemp += "Double: " + strDouble + " ";
    }

    cout << strTemp << endl;
    showCards(BANKER, nzCardsBank, isShowAll, ppBank, ppPlayer);
    showCards(PLAYER, nzCardsPlayer, isShowAll, ppBank, ppPlayer);
    cout << endl;
}

void showCards(Gamer gamer, const std::vector<int> &v, bool isShowAll,
               PointPosibility &ppBank, PointPosibility &ppPlayer){
    switch(gamer){
    case BANKER:
        if(v.size() > 0){
            cout << "Banker: ";
            for(int i = 0; i < v.size(); i++){
                if(!isShowAll && i == 1)
                    cout << "X" << " ";
                else
                    cout << showRealCards(v.at(i)) << " ";
            }
            if(isShowAll)
                cout << ppBank.Max << "/" << ppBank.Min << endl;
            else
                cout << endl;
        }
        break;

    case PLAYER:
        if(v.size() > 0){
            cout << "Player: ";
            for(int i = 0; i < v.size(); i++){
                cout << showRealCards(v.at(i)) << " ";
            }
            cout << ppPlayer.Max << "/" << ppPlayer.Min << endl;
        }
        break;
    }
}

string showRealCards(const int number){
    int nType, nPoint;

    if(number == 0){
        nType = SPADE;
        nPoint = NUMBER_A;
    }else{
        nType = number / 13;
        nPoint = number % 13;
    }

    string sReturn;
    switch(nType){
    case SPADE:
        sReturn = "(S)";
        break;
    case HEART:
        sReturn = "(H)";
        break;
    case DIMAND:
        sReturn = "(D)";
        break;
    case CLUB:
        sReturn = "(C)";
        break;
    }

    switch(nPoint){
    case NUMBER_A:
        sReturn += "A";
        break;
    case NUMBER_2:
        sReturn += "2";
        break;
    case NUMBER_3:
        sReturn += "3";
        break;
    case NUMBER_4:
        sReturn += "4";
        break;
    case NUMBER_5:
        sReturn += "5";
        break;
    case NUMBER_6:
        sReturn += "6";
        break;
    case NUMBER_7:
        sReturn += "7";
        break;
    case NUMBER_8:
        sReturn += "8";
        break;
    case NUMBER_9:
        sReturn += "9";
        break;
    case NUMBER_10:
        sReturn += "10";
        break;
    case FACE_J:
        sReturn += "J";
        break;
    case FACE_Q:
        sReturn += "Q";
        break;
    case FACE_K:
        sReturn += "K";
        break;
    }

    return sReturn;
}

