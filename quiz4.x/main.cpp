#include <iostream>
#include <string>

using namespace std;

enum class MonsterType{
    Ogre,
    Dragon,
    Orc,
    Giant_Spider,
    Slime
};

struct Monster{
    MonsterType type;
    string strName;
    int nHP;
};

void printMonster(Monster m);

int main()
{

    cout << "Quiz 4.x" << endl;

    Monster m1{MonsterType::Ogre, "Torg", 145};
    Monster m2{MonsterType::Slime, "Blurp", 23};

    printMonster(m1);
    printMonster(m2);

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}

void printMonster(Monster m){
    string strType;
    if(m.type == MonsterType::Dragon)
        strType = "Dragon";
    else if(m.type == MonsterType::Giant_Spider)
        strType = "Giant spider";
    else if(m.type == MonsterType::Ogre)
        strType = "Ogre";
    else if(m.type == MonsterType::Orc)
        strType = "Orc";
    else if(m.type == MonsterType::Slime)
        strType = "Slime";
    else
        strType = "Unknown";

    cout << "This " << strType << " is named " << m.strName << " and has " << m.nHP << " health." << endl;
}
