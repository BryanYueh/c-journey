#include <iostream>
#include <vector>

using namespace std;

class Stack{
    vector<int> m_array;
    int m_size;

public:
    void ini(){
        m_array.assign(10, 0);
        m_array.reserve(10);
        m_size = 0;
    }

    void reset(){
        m_array.resize(0);
        m_size = 0;
    }

    bool push(int value){
        if(m_size == 10)
            return false;

        m_array.push_back(value);
        m_size++;

        return true;
    }

    int pop(){
        if (m_size == 0)
            return -1;

        m_array.pop_back();
        m_size--;

        return 0;
    }

    void print(){
        cout << "( ";
        for(auto &i:m_array){
            cout << i << " ";
        }
        cout << " )" << endl;
    }
};

int main()
{
    cout << "Quiz 8.3.2 Stack" << endl;

    Stack stack;
    stack.ini();

	stack.reset();

	stack.print();

	stack.push(5);
	stack.push(3);
	stack.push(8);
	stack.print();

	stack.pop();
	stack.print();

	stack.pop();
	stack.pop();

	stack.print();

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}
