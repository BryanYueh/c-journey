#include <iostream>
#include "Vector3d.h"

using namespace std;

int main()
{
	Point3d p(1.0, 2.0, 3.0);
	Vector3d v(2.0, 2.0, -2.0);

	cout << "Before moving..." << endl;
	v.print();
	p.print();

	p.moveByVector(v);
	cout << "after moving..." << endl;

	v.print();
	p.print();

	cin.clear();
	cin.ignore(32767, '\n');
	cin.get();

	return 0;
}
