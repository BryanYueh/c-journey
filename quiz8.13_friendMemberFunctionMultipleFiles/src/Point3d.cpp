/************************************************************************
*  Project:
*  Function:
*  Describe:
*************************************************************************
*  : FBND
*  : Bryan Yueh
*************************************************************************
*  Copyright 2016 by Burst Entertainment Inc.
************************************************************************/

#include "Point3d.h"

using namespace std;

void Point3d::print()
{
    std::cout << "Point(" << m_x << " , " << m_y << " , " << m_z << ")\n";
}
