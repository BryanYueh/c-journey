#ifndef POINT3D_H
#define POINT3D_H
#include <iostream>

class Vector3d;
class Point3d
{
private:
	double m_x=0.0, m_y=0.0, m_z=0.0;

public:
	Point3d(double x = 0.0, double y = 0.0, double z = 0.0)
		: m_x(x), m_y(y), m_z(z)
	{

	}

	void print();

	void moveByVector(Vector3d &v);
};

#endif // POINT3D_H
