#include <iostream>
#include "Storage.h"
#include "Display.h"

using namespace std;

int main()
{
    Storage storage(5, 6.7);
    Display display(false);

    displayItem(storage);

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}

