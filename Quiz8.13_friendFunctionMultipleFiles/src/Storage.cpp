/************************************************************************
*  Project:
*  Function:
*  Describe:
*************************************************************************
*  : FBND
*  : Bryan Yueh
*************************************************************************
*  Copyright 2016 by Burst Entertainment Inc.
************************************************************************/
#include <iostream>
#include "Storage.h"

using namespace std;

Storage::Storage(int nValue, double dValue)
{
    m_nValue = nValue;
    m_dValue = dValue;
}

void displayItem(Storage &storage)
{
    std::cout << storage.m_nValue << " " << storage.m_dValue << '\n';
}

