#ifndef STORAGE_H
#define STORAGE_H

class Storage
{
    private:
        int m_nValue;
        double m_dValue;
    public:
        Storage(int nValue, double dValue);

        friend void displayItem(Storage& storage);
};

#endif // STORAGE_H
