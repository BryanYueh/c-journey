#include <iostream>
#include <utility>

using namespace std;

int main()
{
    cout << "Quiz 6.4" << endl;

    const int size(9);
    int array[size] = { 6, 3, 2, 9, 7, 1, 5, 4, 8 };

    int nMin;
    int nMax;
    for(int i = 0; i < size - i; i++){
        nMin = i;
        nMax = size - 1 - i;
        for(int j = i + 1; j < size - 1 - i; j++){
            if(array[nMin] > array[j] )
                nMin = j;
            if(array[nMax] < array[j])
                nMax = j;
        }
        swap(array[i], array[nMin]);
        swap(array[size-1-i], array[nMax]);
    }

    cout << "array = ";
    for(int i = 0; i < size; i++){
        cout << array[i];
        if(i < size-1)
            cout << ", ";
        else
            cout << endl;

    }

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}
