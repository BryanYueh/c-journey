#include <iostream>

using namespace std;

struct Fraction{
    int nNumerator;
    int nDenominator;
};

Fraction userInput(string strOrder);
void multiply(Fraction f1, Fraction f2);

int main()
{
    cout << "Quiz 4.7.2" << endl;

    Fraction fractionFirst = userInput(string{"first"});
    Fraction fractionSecond = userInput(string{"second"});

    multiply(fractionFirst, fractionSecond);

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}

Fraction userInput(string strOrder){
    Fraction fraction;
    cout << "Enter the " << strOrder << " numerator: ";
    cin >> fraction.nNumerator;
    cout << "Enter the " << strOrder << " denominator: ";
    cin >> fraction.nDenominator;
    return fraction;
}

void multiply(Fraction f1, Fraction f2){
    cout << f1.nNumerator << "/" << f1.nDenominator << " * " << f2.nNumerator << "/" << f2.nDenominator << " = " <<
    f1.nNumerator * f2.nNumerator << "/" << f1.nDenominator * f2.nDenominator << endl;
}
