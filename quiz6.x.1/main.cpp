#include <iostream>

using namespace std;

enum ItemType{
    HEALTH_POTION,
    TORCH,
    ARROW};
int getItems(ItemType i, const int nzCarry[]);

int main()
{
    cout << "Quiz 6.x.1" << endl;

    int nzCarry[] = {2, 5, 10};

    int nHP = getItems(HEALTH_POTION, nzCarry);
    int nTorch = getItems(TORCH, nzCarry);
    int nArrow = getItems(ARROW, nzCarry);

    cout << "You have " << nHP << " health potions." << endl;
    cout << "You have " << nTorch << " torches." << endl;
    cout << "You have " << nArrow << " arrows." << endl;
    cout << "Total: " << nHP + nTorch + nArrow << " items." << endl;

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}

int getItems(ItemType i, const int nzCarry[]){
    switch(i){
    case HEALTH_POTION:
        return nzCarry[0];
        break;
    case TORCH:
        return nzCarry[1];
        break;
    case ARROW:
        return nzCarry[2];
        break;
    }
}
