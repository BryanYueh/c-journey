/************************************************************************
*  Project:  quiz1.12.2
*  Function:
*  Describe:
*************************************************************************
*  : FBND
*  : Bryan Yueh
*************************************************************************
*  Copyright 2016 by Burst Entertainment Inc.
************************************************************************/

#include <iostream>

using namespace std;

int readNumber(int index){
    cout << "Input #" << index << " integer:";
    int dInput;
    cin >> dInput;
    cout << endl;
    return dInput;
}

void writeAnswer(int dOne, int dTwo){
    cout << dOne << " + " << dTwo << " = " << dOne + dTwo << endl;
}
