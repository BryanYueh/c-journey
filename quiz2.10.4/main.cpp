#include <iostream>
#include "myConst.h"

using namespace std;

double calDistant(double fInitialHeight, double fSec);
void printResult(double fSec, double fHeight);

int main()
{
    cout << "Quiz 2.10.4" << endl;
    cout << "Enter the initial height of the tower in meters: ";
    double fInitialHeight;
    cin >> fInitialHeight;

    printResult(0, calDistant(fInitialHeight, 0));
    printResult(1, calDistant(fInitialHeight, 1));
    printResult(2, calDistant(fInitialHeight, 2));
    printResult(3, calDistant(fInitialHeight, 3));
    printResult(4, calDistant(fInitialHeight, 4));
    printResult(5, calDistant(fInitialHeight, 5));

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}

double calDistant(double fInitialHeight, double fSec){
    double fDistant{MyConst::gravity * fSec * fSec / 2};
    return fInitialHeight - fDistant;
}

void printResult(double fSec, double fHeight){
    if(fHeight > 0)
        cout << "At " << fSec << " seconds, the ball is at height: " << fHeight << " meters" << endl;
    else
        cout << "At " << fSec << " seconds, the ball is on the ground.";
}
