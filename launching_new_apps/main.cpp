#include <iostream>
#include <string>

#if _WIN32
    #include <windows.h>
#elif __linux__
#else
#error "OS not supported!"
#endif

using namespace std;

int main()
{
    cout << "Launching new apps" << endl;

#if _WIN32
    string fn ="D:\\CPPJourney\\threading_prime\\bin\\Release\\threading_prime.exe";
    char* fileName = &fn[0];
    ShellExecute(NULL, "open", fileName, NULL, NULL, SW_SHOWDEFAULT);
#elif __linux__

#endif

    cout << "End of Launching new apps" << endl;

    cin.clear();
	cin.ignore(32767, '\n');
	cin.get();

    return 0;
}
