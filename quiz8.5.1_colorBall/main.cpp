#include <iostream>
#include <string>

using namespace std;

class Ball{
    string m_color;
    double m_radius;

public:
    Ball(){
        m_color = "Black";
        m_radius = 10;
    }
    Ball(string color){
        m_color = color;
        m_radius = 10;
    }
    Ball(double radius){
        m_color = "Black";
        m_radius = radius;
    }
    Ball(string color, double radius){
        m_color = color;
        m_radius = radius;
    }

    void print(){
        cout << "color: " << m_color << ", radius: " << m_radius << endl;
    }

};

int main()
{
    cout << "Quiz 8.5.1 Color Ball" << endl;

    Ball def;
    def.print();

	Ball blue("blue");
	blue.print();

	Ball twenty(20.0);
	twenty.print();

	Ball blueTwenty("blue", 20.0);
	blueTwenty.print();

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}
