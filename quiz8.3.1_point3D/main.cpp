#include <iostream>

using namespace std;

class Point3d{
    double m_x;
    double m_y;
    double m_z;

public:
    void setValues(double x, double y, double z){
        m_x = x;
        m_y = y;
        m_z = z;
    }

    void print(){
        cout << "<" <<  m_x << ", " <<
                        m_y << ", " <<
                        m_z << ">" << endl;
    }
};


int main()
{
    cout << "Quiz 8.3 Point3D" << endl;


    Point3d point;
    point.setValues(1.0, 2.0, 3.0);

    point.print();

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}
