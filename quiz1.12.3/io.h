/************************************************************************
*  Project:  quiz1.12.3
*  Function:
*  Describe:
*************************************************************************
*  : FBND
*  : Bryan Yueh
*************************************************************************
*  Copyright 2016 by Burst Entertainment Inc.
************************************************************************/

#ifndef IO_H_INCLUDED
#define IO_H_INCLUDED

int readNumber(int index);
void writeAnswer(int dOne, int dTwo);

#endif // IO_H_INCLUDED
