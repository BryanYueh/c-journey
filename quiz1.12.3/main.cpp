#include <iostream>
#include "io.h"

using namespace std;

int main()
{
    cout << "Quiz 1.12.3" << endl;

    int dOne = readNumber(1);
    int dTwo = readNumber(2);

    writeAnswer(dOne, dTwo);

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}
