#ifndef CONSTANTS_H_INCLUDED
#define CONSTANTS_H_INCLUDED

namespace myConstants
{
    const double gravity(9.8); // in meters/second squared
}

#endif // CONSTANTS_H_INCLUDED
