#include <iostream>

using namespace std;

int main()
{
    cout << "Quiz 6.3.3" << endl;

    const int numStudents(5);
    int scores[numStudents] = { 84, 92, 76, 81, 56 };

    int nIndexMax(0);
    for(int i = 0; i < numStudents; i++){
        if(scores[i] > scores[nIndexMax])
            nIndexMax = i;
    }

    cout << "The best score was scores[" << nIndexMax << "]=" << scores[nIndexMax] << endl;

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}
