#include <iostream>
#include <string>

using namespace std;

int main()
{
    cout << "Quiz 4.4b" << endl;

    cout << "Enter your full name: ";
    string strName;
    getline(cin, strName);

    cout << "Enter your age: ";
    int nAge;
    cin >> nAge;

    int nLetters;
    nLetters = strName.length();
    double nLived{static_cast<double>(nAge) / nLetters};

    cout << "You\'ve lived " << nLived << " years for each letter in your name." << endl;

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}
