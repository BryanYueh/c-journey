#include <iostream>

using namespace std;
bool isOdds(int value);

int main()
{
    cout << "Quiz 3.2.2" << endl;
    cout << "Enter an integer: ";
    int nInput;
    cin >> nInput;
    if(isOdds(nInput))
        cout << nInput << " is an odd.";
    else
        cout << nInput << " is an even.";

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}

bool isOdds(int value){
    if(value % 2 != 0)
        return true;
    else
        return false;
}
