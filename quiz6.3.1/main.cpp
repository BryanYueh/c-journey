#include <iostream>

using namespace std;

int main()
{
    cout << "Quiz 6.3.1" << endl;

    const int arrayLength(9);
    int array[arrayLength] = { 4, 6, 7, 3, 8, 2, 1, 9, 5 };

    for(int i = 0; i < arrayLength; i++){
        cout << "array[" << i << "] = " << array[i] << endl;
    }

    return 0;
}
