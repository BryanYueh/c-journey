#include <iostream>
#include <thread>
#include <set>
#include <mutex>
#include <vector>

using namespace std;

mutex mutexPrimeContainer;
mutex mutexPrint;

class PrimeContainer{
private:
    set<int> setContain;
public:
    unsigned int getTotal(){
        return setContain.size();
    }
    void insert(int nValue){
        mutexPrimeContainer.lock();
        setContain.insert(nValue);
        mutexPrimeContainer.unlock();
    }
    void dump(){
        set<int>::iterator it;
        int i = 0;
        for(it = setContain.begin(); it != setContain.end(); ++it){
            ++i;
            cout << i << "-" << *it << " ";
        }
    }
};

class Prime{
private:
    PrimeContainer& cPC;
    int nMin;
    int nMax;
    int nTotalTemp;
    void startD2(){
        for(int i = nMin; i <= nMax; i++){
            int nMax2 = 0;
            if(i % 2 == 0){
                nMax2 = i / 2;
            }else{
                nMax2 = i / 2 + 1;
            }
            if(nMax2 < i){
                nMax2 = i;
            }
            for(int j = 2; j <= nMax2; j++){
                if(i % j == 0){
                    nTotalTemp++;
                }
                if(nTotalTemp > 1){
                    break;
                }
            }
            if(nTotalTemp <= 1){
                cPC.insert(i);
            }
            nTotalTemp = 0;
        }
    }
public:
    Prime(int min, int max, PrimeContainer& pc):nMin(min), nMax(max), cPC(pc), nTotalTemp(0){}
    void operator()(){
        thread::id id = this_thread::get_id();
        mutexPrint.lock();
        cout << "thread " << id << " nMin=" << nMin << endl;
        cout << "thread " << id << " nMax=" << nMax << endl;
        cout << "thread " << id << " nTotalTemp=" << nTotalTemp << endl;
        mutexPrint.unlock();
        startD2();
    }
};

class PrimeRAII{
private:
    thread& m_thread;
public:
    PrimeRAII(thread& th):m_thread(th){}
    ~PrimeRAII(){
        if(m_thread.joinable()){
            mutexPrint.lock();
            cout << "thread id: " << m_thread.get_id() << " join!" << endl;
            mutexPrint.unlock();
            m_thread.join();
        }
    }
};

void startThreading(int max, PrimeContainer &cpc){
    int nDivide = 10;
    int nRange = max / nDivide;

    vector<int> vTop;
    for(int i = 0; i < nDivide -1; ++i){
        vTop.push_back(nRange * i + nRange);
    }
    vTop.push_back(max);

    vector<int> vBottom;
    for(int i = 0; i < nDivide; ++i){
        if(i==0){
            vBottom.push_back(2);
        }else{
            vBottom.push_back(nRange * i + 1);
        }

    }

    vector<thread> vThread;
    vector<PrimeRAII> vPrimeRAII;

    for(int i = 0; i < nDivide; ++i){
        Prime p(vBottom.at(i), vTop.at(i), cpc);
        vThread.push_back(thread(p));
    }

    for(int i = 0; i < nDivide; ++i){
        PrimeRAII raii(vThread.at(i));
        vPrimeRAII.push_back(raii);
    }
}

int main()
{
    cout << "threading prime" << endl;

    PrimeContainer cpc;

    startThreading(500000, cpc);

    cpc.dump();
    cout << endl << "Total: " << cpc.getTotal() << endl;

    cout << "End of threading prime" << endl;

    cin.clear();
	cin.ignore(32767, '\n');
	cin.get();

    return 0;
}
