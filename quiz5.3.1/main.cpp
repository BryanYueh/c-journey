#include <iostream>
#include <cstdlib>

using namespace std;
double calculate(int n1, char op, int n2);

int main()
{
    cout << "Quiz 5.3.1" << endl;

    cout << "Input the first integer: ";
    int n1;
    cin >> n1;

    cout << "Input one of the operators(+,-,*,/): ";
    char cOp;
    cin >> cOp;

    cout << "Input the second integer: ";
    int n2;
    cin >> n2;

    double dResult = calculate(n1, cOp, n2);

    if(dResult != -0.6969)
        cout << n1 << " " << cOp << " " << n2 << " = " << dResult << endl;
    else
        exit(0);

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}

double calculate(int n1, char op, int n2){
    switch(op){
    case '+':
        return static_cast<double>(n1 + n2);
        break;
    case '-':
        return static_cast<double>(n1 - n2);
        break;
    case '*':
        return static_cast<double>(n1 * n2);
        break;
    case '/':
        return static_cast<double>(n1 / n2);
        break;
    default:
        cout << "What the fuck did you input the operator?" << endl;
        return -0.6969;
        break;
    }
}
