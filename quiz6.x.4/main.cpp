#include <iostream>

using namespace std;
void printChar(const char *str);

int main()
{
    cout << "Quiz 6.x.4" << endl;

    printChar("Hello world!");

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}

void printChar(const char *str){
    while(*str != '\0'){
        cout << *str << endl;
        str++;
    }
}
