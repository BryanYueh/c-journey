#include <iostream>

using namespace std;

double calculate(double one, double two, char op);

int main()
{
    cout << "Quiz 2.10.3" << endl;

    cout << "Enter a number: ";
    double fInput1;
    cin >> fInput1;
    cout << "Enter second number: ";
    double fInput2;
    cin >> fInput2;
    cout << "Enter one of the following:+, -, *, /: ";
    char chInput;
    cin >> chInput;

    cout << fInput1 << " " << chInput << " " << fInput2 << " = " << calculate(fInput1, fInput2, chInput) << endl;

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}

double calculate(double one, double two, char op){
    if(op == '+')
        return one + two;
    else if(op == '-')
        return one - two;
    else if(op == '*')
        return one * two;
    else if(op == '/')
        return one / two;
    else
        return -1;
}
