#include <iostream>

using namespace std;

int doubleNumber(int x){
    return 2 * x;
}

int main()
{
    cout << "This is 1.4a quiz." << endl;
    cout << "Input a integer: ";
    int dInput;
    cin >> dInput;
    cout << endl;
    cout << "The answer is:" << doubleNumber(dInput) << " " << endl;
    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();
    return 0;
}
