#include <iostream>

using namespace std;

int main()
{
    cout << "Quiz 5.5.2" << endl;

    int i(0);
    char c('a');
    while(i < 26){
        cout << c << " = " << static_cast<int>(c) << endl;
        i++;
        c++;
    }

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}
