#include <iostream>

using namespace std;

class RGBA{
    int m_R;
    int m_G;
    int m_B;
    int m_A;

public:
    RGBA():m_R(0), m_G(0), m_B(0), m_A(255){}
    RGBA(int r, int g, int b):RGBA(){
        m_R = r;
        m_G = g;
        m_B = b;
    }
    RGBA(int a):RGBA(){
        m_A = a;
    }
    void print(){
        cout << "r=" << m_R << " g=" << m_G << " b=" << m_B << " a=" << m_A << endl;
    }
};

int main()
{
    cout << "Quiz8.5.a RGBA" << endl;

    RGBA init;
    init.print();

    RGBA rgb1(1, 11, 111);
	rgb1.print();

	RGBA rgb2(127);
	rgb2.print();

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}
