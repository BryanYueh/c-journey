#ifndef POINT2D_H
#define POINT2D_H
#include <iostream>
#include <math.h>

class Point2D
{
    public:
        Point2D();
        Point2D(double x, double);
        print();
        double distanceTo1(Point2D p);
        friend double distanceTo2(Point2D p1, Point2D p2);
    private:
        double m_x;
        double m_y;
};

#endif // POINT2D_H
