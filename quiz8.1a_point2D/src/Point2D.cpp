/************************************************************************
*  Project:
*  Function:
*  Describe:
*************************************************************************
*  : FBND
*  : Bryan Yueh
*************************************************************************
*  Copyright 2016 by Burst Entertainment Inc.
************************************************************************/

#include "Point2D.h"

using namespace std;

Point2D::Point2D()
{
    m_x = 0.0;
    m_y = 0.0;
}

Point2D::Point2D(double x, double y){
    m_x = x;
    m_y = y;
}

Point2D::print()
{
    cout << "Point2D(" << m_x << ", " << m_y << ")" << endl;
}

double Point2D::distanceTo1(Point2D p){
    return sqrt((m_x - p.m_x)*(m_x - p.m_x) + (m_y - p.m_y)*(m_y - p.m_y));
}

//friend function
double distanceTo2(Point2D p1, Point2D p2){
    return sqrt((p1.m_x - p2.m_x)*(p1.m_x - p2.m_x) + (p1.m_y - p2.m_y)*(p1.m_y - p2.m_y));
}
