#include <iostream>
#include "Point2D.h"

using namespace std;

int main()
{
    cout << "Quiz 8.1a point2D" << endl;

    Point2D first;
    Point2D second(3.0, 4.0);
    first.print();
    second.print();

    cout << "T1 Distance between two points: " << first.distanceTo1(second) << endl;

    cout << "T2 Distance between two points: " << distanceTo2(first, second) << endl;

	cin.clear();
	cin.ignore(32767, '\n');
	cin.get();

    return 0;
}
