#include <iostream>

int readNumber(int index);
void writeAnswer(int dOne, int dTwo);

using namespace std;

int main()
{
    cout << "Quiz 1.12.1" << endl;

    int dOne = readNumber(1);
    int dTwo = readNumber(2);

    writeAnswer(dOne, dTwo);

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}

int readNumber(int index){
    cout << "Input #" << index << " integer:";
    int dInput;
    cin >> dInput;
    cout << endl;
    return dInput;
}

void writeAnswer(int dOne, int dTwo){
    cout << dOne << " + " << dTwo << " = " << dOne + dTwo << endl;
}
