#include <iostream>

using namespace std;

int main()
{
    cout << "Quiz 6.12a" << endl;

    const string strNames[] = {"Albert", "Bryan", "Charles", "Denny", "Edison",
                      "Frank", "Gary", "Helen", "Idiot", "Jack"};

    string strInput;
    bool bFound = false;
    do{
        cout << "Enter a name(q to Quit): ";
        cin.clear();
        cin >> strInput;

        for(const auto &n : strNames){
            if(n == strInput){
                bFound = true;
                break;
            }
        }

        if(strInput == "q")
            break;

        if(bFound)
            cout << strInput << " was found." << endl;
        else
            cout << strInput << " was not found." << endl;

        bFound = false;
    }while(strInput != "q");

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}
