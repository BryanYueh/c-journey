#include <iostream>
#include <string>

using namespace std;

enum class Animal{
    PIG,
    CHICKEN,
    GOAT,
    CAT,
    DOG,
    OSTRICH
};

string getAnimalName(Animal animal);
void printNumberOfLegs(Animal animal);

int main()
{
    cout << "Quiz 5.3.2" << endl;

    Animal chicken(Animal::CHICKEN);
    Animal pig(Animal::PIG);
    printNumberOfLegs(chicken);
    printNumberOfLegs(pig);

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}

string getAnimalName(Animal animal){
    switch(animal){
    case Animal::CAT:
        return "cat";
        break;
    case Animal::CHICKEN:
        return "chicken";
        break;
    case Animal::DOG:
        return "dog";
        break;
    case Animal::GOAT:
        return "goat";
        break;
    case Animal::OSTRICH:
        return "ostrich";
        break;
    case Animal::PIG:
        return "pig";
        break;
    default:
        return "alien";
        break;
    }
}

void printNumberOfLegs(Animal animal){
    switch(animal){
    case Animal::CHICKEN:
    case Animal::OSTRICH:
        cout << "A " << getAnimalName(animal) << " has 2 legs." << endl;
        break;
    case Animal::CAT:
    case Animal::DOG:
    case Animal::GOAT:
    case Animal::PIG:
        cout << "A " << getAnimalName(animal) << " has 4 legs." << endl;
        break;
    default:
        cout << "What the fuck did you input?" << endl;
        break;
    }
}
