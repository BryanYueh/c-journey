#include <iostream>

using namespace std;

int main()
{
    cout << "Quiz 6.9a" << endl;

    cout << "How many names would you like to enter? ";
    int nInput;
    cin >> nInput;

    string *names = new string[nInput];
    for(int i = 0; i < nInput; i++){
        cout << "Enter name #" << i+1 << ": ";
        cin.clear();
        cin.ignore(32767, '\n');
        cin >> names[i];
    }

    int nMin;
    int nMax;
    for(int i = 0; i < nInput - i; i++){
        nMin = i;
        nMax = nInput - 1 - i;
        for(int j = i + 1; j < nInput - 1 - i; j++){
            if(names[nMin] > names[j] )
                nMin = j;
            if(names[nMax] < names[j])
                nMax = j;
        }
        swap(names[i], names[nMin]);
        swap(names[nInput-1-i], names[nMax]);
    }

    cout << "Here is your sorted list:" << endl;
    for(int i = 0; i < nInput; i++){
        cout << "Name #" << i+1 << ": " << names[i] << endl;
    }

    delete[] names;

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}
