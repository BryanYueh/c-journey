#include <iostream>

using namespace std;

class Vector3d
{
private:
	double m_x = 0.0, m_y = 0.0, m_z = 0.0;

public:
	Vector3d(double x = 0.0, double y = 0.0, double z = 0.0)
		: m_x(x), m_y(y), m_z(z)
	{

	}

	void print()
	{
		std::cout << "Vector(" << m_x << " , " << m_y << " , " << m_z << ")\n";
	}

	friend class Point3d;
};

class Point3d
{
private:
	double m_x=0.0, m_y=0.0, m_z=0.0;

public:
	Point3d(double x = 0.0, double y = 0.0, double z = 0.0)
		: m_x(x), m_y(y), m_z(z)
	{

	}

	void print()
	{
		std::cout << "Point(" << m_x << " , " << m_y << " , " << m_z << ")\n";
	}

	void moveByVector(Vector3d &v)
	{
		m_x += v.m_x;
		m_y += v.m_y;
		m_z += v.m_z;
	}
};

int main()
{
	Point3d p(1.0, 2.0, 3.0);
	Vector3d v(2.0, 2.0, -2.0);
	cout << "Before moving..." << endl;
	v.print();
	p.print();

	cout << "After moving..." << endl;
	p.moveByVector(v);
	v.print();
	p.print();

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

	return 0;
}
