#include <iostream>
namespace animal{
    enum Animals{
        CHICKEN,
        DOG,
        CAT,
        ELEPHANT,
        DUCK,
        SNAKE,
        MAX_ANIMALS
    };
}

using namespace std;
using namespace animal;

int main()
{
    cout << "Quiz 6.2.2" << endl;

    int legs[Animals::MAX_ANIMALS] = {2, 4, 4, 4, 2, 0};

    cout << "The elephant has " << legs[Animals::ELEPHANT] << " legs." << endl;

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}
