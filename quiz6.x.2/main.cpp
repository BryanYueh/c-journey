#include <iostream>
#include <vector>

using namespace std;

struct Student{
    string name;
    int grade;
};

void mySwap(Student &st1, Student &st2);
int main()
{
    cout << "Quiz 6.x.2" << endl;

    cout << "How many students: ";
    int nInputStudent;
    cin >> nInputStudent;

    vector<Student> sts;
    sts.resize(nInputStudent);

    for(int i = 0; i < nInputStudent; i++){
        cout << "The #" << i+1 << " name: ";
        cin.clear();
        cin >> sts[i].name;
        cout << "The #" << i+1 << " grade: ";
        cin.clear();
        cin >> sts[i].grade;
    }

    cout << "The original array: " << endl;
    int nIndex = 0;
    for(Student &s:sts){
        cout << "sts[" << nIndex << "].name= " << s.name << endl;
        cout << "sts[" << nIndex << "].grade= " << s.grade << endl;
        nIndex++;
    }
    cout << endl;

    int nMin;
    for(int i = 0; i < nInputStudent; i++){
        nMin = i;
        for(int j = i+1; j < nInputStudent; j++){
            if(sts[j].grade < sts[nMin].grade)
                nMin = j;
        }

        mySwap(sts[i], sts[nMin]);
        cout << i+1 << " swap" << endl;
        for(int i = 0; i < nInputStudent; i++){
            cout << "sts[" << i << "].name= " << sts[i].name << endl;
            cout << "sts[" << i << "].grade= " << sts[i].grade << endl;
        }
        cout << endl;
    }

    cout << "The result:" << endl;
    for(Student &st:sts){
        cout << st.name << " got a grade of " << st.grade << endl;
    }

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}

void mySwap(Student &st1, Student &st2){
    Student s;
    s.name = st1.name;
    s.grade = st1.grade;
    st1.name = st2.name;
    st1.grade = st2.grade;
    st2.name = s.name;
    st2.grade = s.grade;
}
