#include <iostream>

using namespace std;

class Chaining{
    int m_str = 25;
    int m_con = 25;
    int m_int = 10;
    int m_agi = 15;
    int m_weapon = 100;
    int m_atk = 0;

public:
    Chaining& baseATK(){
        m_atk += m_str + m_con + m_int + m_agi;
        return *this;
    }
    Chaining& extraATK(){
        m_atk += m_weapon;
        return *this;
    }
    int getAtk(){return m_atk;}
    void print(){
        cout << "ATK = " << getAtk() << endl;
    }
};

int main()
{
    cout << "Quiz8.8 Chaining object" << endl;

    Chaining chain;
    chain.baseATK().extraATK().print();

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}
