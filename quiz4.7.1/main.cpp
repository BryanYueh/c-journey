#include <iostream>

using namespace std;

struct Advertising{
        int nAds;
        double dPercentage;
        double dAverageEarnPerClick;
};

void PrintTotalEarn(Advertising ads);

int main()
{
    cout << "Quiz 4.7.1" << endl;

    Advertising ads;
    cout << "How many ads were shown today? ";
    cin >> ads.nAds;
    cout << "What percentage of users clicked on the ads? ";
    cin >> ads.dPercentage;
    cout << "What was the average earnings per click? ";
    cin >> ads.dAverageEarnPerClick;

    PrintTotalEarn(ads);

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}

void PrintTotalEarn(Advertising ads){
    cout << "Total Earnings: $" <<
    static_cast<double>(ads.nAds) * (ads.dPercentage / 100.0) * ads.dAverageEarnPerClick << endl;
}

