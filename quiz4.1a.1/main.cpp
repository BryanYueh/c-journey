#include <iostream>

using namespace std;
bool isSwap(int n1, int n2);

int main()
{
    cout << "Quiz 4.1a.1" << endl;

    cout << "Enter an integer: ";
    int nInput1;
    cin >> nInput1;
    cout << "Enter another integer: ";
    int nInput2;
    cin >> nInput2;

    if(isSwap(nInput1, nInput2)){
        cout << "The smaller value is " << nInput2 << endl;
        cout << "The larger value is " << nInput1 << endl;
    }else{
        cout << "The smaller value is " << nInput1 << endl;
        cout << "The larger value is " << nInput2 << endl;
    }

    cin.clear();
    cin.ignore(32767, '\n');
    cin.get();

    return 0;
}

bool isSwap(int n1, int n2){
    if(n1 > n2){
        cout << "Swapping the values" << endl;
        return true;
    }else{
        return false;
    }
}
